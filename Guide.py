__author__ = 'jamescarthew'

from PIL import Image
from LocalPath import LOCAL_PATH


class Guide(object):
    def __init__(self, name,  dialogue):
        self.name = name
        self.avImagePath = LOCAL_PATH+"/assets/prince_gumball.png"
        self.dialogue = dialogue

    def get_avatar(self):
        image = Image.open(self.avImagePath)
        return image

    # Questions from guide
    # Dialogue