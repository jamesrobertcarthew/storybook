__author__ = 'jamescarthew'

from PIL import Image
from LocalPath import LOCAL_PATH

class Traveller(object):

    def __init__(self, name, avatar_ref):
        self.name = name
        self.avatar_ref = avatar_ref
        self.x = 0
        self.y = 0

    def image_path(self):
        if self.avatar_ref == 1:
                path = LOCAL_PATH+"/assets/princess_alice.png"
        elif self.avatar_ref == 2:
                path =  LOCAL_PATH+"/assets/princess_ariel.png"
        elif self.avatar_ref == 3:
                path =  LOCAL_PATH+"/assets/princess_gwen.png"
        elif self.avatar_ref == 4:
                path =  LOCAL_PATH+"/assets/princess_jasmine.png"
        return path

    def get_avatar(self):
        self.image = Image.open(self.image_path())
        return self.image
    def get_name(self):
        return self.name

    def get_dim(self):
        image = self.get_avatar()
        dim_x, dim_y = image.size
        self.x = dim_x
        self.y = dim_y
        return self.x, self.y

    def get_x_dimension(self):
        self.get_dim()
        return self.x

    def get_y_dimension(self):
        self.get_dim()
        return self.y



    # Traveller Dialogue
    # Traveller Path (the continuous path through the installation,  not the journey progression)
    # Traveller Spells
    # Multiple Traveller capabilities (for 2 or more children)
    # Traveller avatar dimensions