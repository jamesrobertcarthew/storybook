__author__ = 'jamescarthew'


from PIL import Image
from LocalPath import LOCAL_PATH
class Collage(object):
    def __init__(self):
        self.handle = self
    def center_image_vertical(self, foregnd_x, backgnd_x):
        return (backgnd_x - foregnd_x)/2

    def get_dim_first_third_x(self, foregnd_y, backgnd_y):
        return backgnd_y/3 - foregnd_y

    def get_dim_last_third_x(self, foregnd_y, backgnd_y):
        return 2*backgnd_y/3 + foregnd_y/2

    def paste_image_choose_third(self, foregnd, backgnd, third):
        foregnd_x, foregnd_y = foregnd.size
        backgnd_x, backgnd_y = backgnd.size
        image_process = backgnd
        if third == 1:
            place = self.get_dim_first_third_x
        elif third == 2:
            place = self.get_dim_last_third_x

        image_process.paste(foregnd, (place(foregnd_x, backgnd_x), self.center_image_vertical(foregnd_y, backgnd_y)), foregnd)
        return image_process

    def paste_on_center_frame(self, foregnd, backgnd):
        foregnd_x, foregnd_y = foregnd.size
        backgnd_x, backgnd_y = backgnd.size
        image_process = backgnd
        image_process.paste(foregnd, (0, (backgnd_y-foregnd_y)/2), foregnd)
        return image_process