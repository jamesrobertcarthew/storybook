__author__ = 'jamescarthew'


from PIL import Image
from LocalPath import LOCAL_PATH

class Journey(object):
    def __init__(self, nodeSequence):
        self.nodeSequence = nodeSequence
        self.seqStep = 0

    def get_current_kingdom(self):
        currentKingdom = self.nodeSequence[self.seqStep]
        self.seqStep = self.seqStep+1
        return currentKingdom

