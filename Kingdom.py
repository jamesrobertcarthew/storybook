__author__ = 'jamescarthew'


from PIL import Image
from LocalPath import LOCAL_PATH

class Kingdom(object):
    def __init__(self):
        self.name = ""
        self.kingdom_ref = 0
        self.avatar_place_x = 0
        self.avatar_place_y = 0

    def setup_kingdom(self):
        if self.kingdom_ref == 1:
            path = LOCAL_PATH+"/assets/desert_kingdom.png"
            self.name = "Desert Kingdom"
        elif self.kingdom_ref == 2:
            path = LOCAL_PATH+"/assets/fire_kingdom.png"
            self.name = "Fire Kingdom"
        elif self.kingdom_ref == 3:
            path = LOCAL_PATH+"/assets/goblin_kingdom.png"
            self.name = "Goblin Kingdom"
        elif self.kingdom_ref == 4:
            path = LOCAL_PATH+"/assets/ice_kingdom.png"
            self.name = "Ice Kingdom"
        return path

    def update_kingdom(self, new_kingdom):
        self.kingdom_ref = new_kingdom

    def get_image(self):
        image = Image.open(self.setup_kingdom())
        return image

    def get_name(self):
        return self.name

    def avatar_position(self):
        #  Provides the coordinates to place the avatar on the current kingdom image
        #  Allows for art direction of various pages / illustration composition
#
        # if self.kingdomRef == 1:
        # elif self.kingdomRef == 2:
        # elif self.kingdomRef == 3:
        # elif self.kingdomRef == 4:
        image = self.get_image()
        dim_x, dim_y = image.size
        self.avatar_place_x = 2*dim_x/3
        self.avatar_place_y = dim_y/2
        return self.avatar_place_x, self.avatar_place_y

    def avatar_position_x(self):
        self.avatar_position()
        return self.avatar_place_x

    def avatar_position_y(self):
        self.avatar_position()
        return self.avatar_place_y